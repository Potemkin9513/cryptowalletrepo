﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CryptoWallet.Model.Exchanges
{
    public class YobitExchange : Exchange
    {
        private string _url = "https://yobit.net/api/3/{0}/";

        public override string Name { get; } = "Yobit";

        protected override IDictionary<string, string> CurrencyPairs { get; set; }

        public YobitExchange() : base()
        {
        }

        protected override void SetCurrencyPairs()
        {
            CurrencyPairs = new Dictionary<string, string> {                
                { "rub", "rur" },
                { "usd", "usd" },
                { "btc", "btc" }
            };

            bool isWrongCurrency =
                CurrencyPairs.Any(title => !CurrencyStringList.Contains(title.Key));
            bool isAllExists = CurrencyStringList
                .ToList()
                .TrueForAll(
                    currency => CurrencyPairs
                    .Select(pair => pair.Key)
                    .Contains(currency)
                );
            if (CurrencyPairs.Count != CurrencyStringList.Count || isWrongCurrency || !isAllExists)
                throw new Exception();
        }

        protected override void FillCoinsPrices()
        {
            var apiName = "ticker";

            var coinsUrlEncode = CoinsUrlEncode();
            if (coinsUrlEncode == string.Empty)
                return;

            StringBuilder address = new StringBuilder(string.Format(_url, apiName));

            address.Append(coinsUrlEncode);

            var text = GetHttpRequestText(address.ToString());

            var pairs = JsonConvert.DeserializeObject<Dictionary<string, JToken>>(text);
            foreach (var coin in pairs) {
                var parameters = JsonConvert.DeserializeObject<Dictionary<string, string>>(coin.Value.ToString());

                var sellPrice = Convert.ToDouble(parameters["sell"].Replace('.', ','));
                var title = coin.Key.Split('_')[0];
                Coins.ToList().Find(c => c.Title == title).SellPrice = sellPrice;
            }

            CalculateExchangeBalance();
        }

        private string CoinsUrlEncode()
        {
            if (Coins.Count == 0)
                return string.Empty;

            var stringBuilder = new StringBuilder();
            var currency = CurrencyPairs[CoinsManager.SelectedCurrencyCommonTitle];
            foreach (Coin coin in Coins) {
                stringBuilder.Append($"{coin.Title}_{currency}-");
            }
            stringBuilder.Remove(stringBuilder.Length - 1, 1);

            return stringBuilder.ToString();
        }

        protected override List<KeyValuePair<string, string>> GetAllPairs()
        {
            var apiName = "info";
            var text = GetHttpRequestText(string.Format(_url, apiName));

            JToken a = (JToken)JsonConvert.DeserializeObject(text);
            Dictionary<string, JToken> pairInfo =
                JsonConvert.DeserializeObject<Dictionary<string, JToken>>(a["pairs"].ToString());

            List<KeyValuePair<string, string>> pairs = new List<KeyValuePair<string, string>>();

            foreach (var pair in pairInfo) {
                var coinInPair = pair.Key.Split('_');
                pairs.Add(new KeyValuePair<string, string>(coinInPair[0], coinInPair[1]));
            }

            return pairs;
        }
    }
}
