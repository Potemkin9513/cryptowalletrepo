﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CryptoWallet.Model.Exchanges
{
    public abstract class Exchange : ObservableObject
    {
        public abstract String Name { get; }

        public IList<Coin> Coins { get; set; } = new List<Coin>();

        public static ObservableCollection<String> CurrencyStringList { get; set; }

        /*
         *  The correspondence between the currency represented in UI and the currency in the stock exchange
        */
        protected abstract IDictionary<String, String> CurrencyPairs { get; set; }

        protected List<KeyValuePair<String, String>> _allPairs;

        public static EventHandler QueryStarted;
        public static EventHandler QueryEnded;

        public EventHandler ExchangeBalanceChanged;

        static Exchange()
        {
            CurrencyStringList = new ObservableCollection<String>() {
                "rub", "usd", "btc"
            };
        }

        public Exchange()
        {
            Coin.EquivalentInCurrencyChangedEvent += (s, e) => CalculateExchangeBalance();

            SetCurrencyPairs();
        }

        #region Properties

        private List<String> _instanceCoinsStrings;
        public List<String> InstanceCoinsStrings {
            get { return _instanceCoinsStrings; }
            private set {
                _instanceCoinsStrings = value;
        
                RaisePropertyChanged(() => InstanceCoinsStrings);
            }
        }

        private double _exchangeBalance;
        public double ExchangeBalance {
            get { return _exchangeBalance; }
            set {
                _exchangeBalance = value;

                ExchangeBalanceChanged?.Invoke(null, EventArgs.Empty);

                RaisePropertyChanged(() => ExchangeBalance);
            }
        }

        #endregion

        protected abstract void SetCurrencyPairs();

        public void FillCoinsPricesWithLoading()
        {
            QueryStarted?.Invoke(null, EventArgs.Empty);
            FillCoinsPrices();
            QueryEnded?.Invoke(null, EventArgs.Empty);
        }

        protected abstract void FillCoinsPrices();

        protected abstract List<KeyValuePair<String, String>> GetAllPairs();

        public void AddCoin(Coin coin)
        {
            Coins.Add(coin);
        }

        public void RemoveCoin(Coin coin)
        {
            var coinTitle = coin.Title;
            
            Coins.Remove(coin);
            CalculateExchangeBalance();
        }

        protected static String GetHttpRequestText(String address)
        {
            WebRequest request = WebRequest.Create(address);
            WebResponse response = request.GetResponse();
            String text = String.Empty;
            using (Stream stream = response.GetResponseStream()) {
                using (StreamReader reader = new StreamReader(stream)) {
                    text = reader.ReadToEnd();
                }
            }
            response.Close();
                
            return text;
        }

        protected void CalculateExchangeBalance()
        {
            ExchangeBalance = Coins.Sum(coin => coin.EquivalentInCurrency);
        }

        public void DownloadAllPairs()
        {
            QueryStarted?.Invoke(null, EventArgs.Empty);

            _allPairs = GetAllPairs();
            InstanceCoinsStrings = _allPairs
                .Select(pair => pair.Key.ToString())
                .Distinct()
                .ToList();


            InstanceCoinsStrings.Sort();
            QueryEnded?.Invoke(null, EventArgs.Empty);
        }


        public override String ToString() => Name;
    }
} 
