﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CryptoWallet.Model
{
    public class FileIOService : IOService
    {
        private BinaryFormatter _formatter = new BinaryFormatter();

        private readonly string _filePath;

        public FileIOService(string filePath)
        {
            _filePath = filePath;
        }
        
        public void Write(IEnumerable<CoinToStore> coins)
        {
            using (FileStream stream = new FileStream(_filePath, FileMode.Open)) {
                _formatter.Serialize(stream, new List<CoinToStore>(coins));
            }
        }

        IEnumerable<CoinToStore> IOService.Read()
        {
            using (FileStream stream = new FileStream(_filePath, FileMode.OpenOrCreate)) {
                return (IEnumerable<CoinToStore>)_formatter.Deserialize(stream);
            }
        }
    }
}
