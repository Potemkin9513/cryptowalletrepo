using CryptoWallet.Model;
using CryptoWallet.Model.Exchanges;
using CryptoWallet.Model.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Data;
using System.Windows.Input;
using System.Configuration;

namespace CryptoWallet.ViewModel
{
    // TODO: rewrite work with the db to uniformity (SqlAdapter, ef, DbCommand etc)
    // TODO: loading from db animation
    // TODO: documentation
    // TODO: logging
    // TODO: add dialog messages
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            var filePath = Path.GetDirectoryName(
                    Path.GetDirectoryName(
                        Path.GetDirectoryName(
                            Directory.GetCurrentDirectory()
                        )
                    )
                ) + "\\Data\\CoinsData";
            IOService ioService = new FileIOService(filePath);

            //string connectionString = ConfigurationManager
            //    .ConnectionStrings["DefaultConnection"]
            //    .ConnectionString;
            //IOService ioService = new DatabaseIOService(connectionString);

            CoinsManager = new CoinsManager(ioService);

            Exchange.QueryStarted += (s, e) => IsLoading = true; ;
            Exchange.QueryEnded += (s, e) => IsLoading = false; ;
        }

        #region Properties

        private CoinsManager _coinsManager;
        public CoinsManager CoinsManager {
            get { return _coinsManager; }
            set {
                _coinsManager = value;
                RaisePropertyChanged(() => CoinsManager);
            }
        }

        private Exchange _selectedExchange;
        public Exchange SelectedExchange {
            get {
                return _selectedExchange;
            }
            set {
                if (_selectedExchange == value) {
                    return;
                }

                _selectedExchange = value;

                if (_selectedExchange.InstanceCoinsStrings == null) {
                    Task.Run(() => {
                        _selectedExchange.DownloadAllPairs();
                        ToBeAddedCoinTitle = _selectedExchange.InstanceCoinsStrings?.First();
                    });
                } else {
                    ToBeAddedCoinTitle = _selectedExchange.InstanceCoinsStrings?.First();
                }

                RaisePropertyChanged(() => SelectedExchange);
            }
        }


        private Coin _selectedCoin;
        public Coin SelectedCoin {
            get { return _selectedCoin; }
            set {
                if (_selectedCoin == value) {
                    return;
                }

                _selectedCoin = value;
            }
        }

        private string _toBeAddedCoinTitle;
        public string ToBeAddedCoinTitle {
            get { return _toBeAddedCoinTitle; }
            set {
                if (_toBeAddedCoinTitle == value) {
                    return;
                }

                _toBeAddedCoinTitle = value;

                RaisePropertyChanged(() => ToBeAddedCoinTitle);
            }
        }

        private bool _isLoading;
        public bool IsLoading {
            get { return _isLoading; }
            set {
                _isLoading = value;
                RaisePropertyChanged(() => IsLoading);
            }
        }

        // private bool _foo = true;
        // public bool Foo {
        //     get { return _foo; }
        //     set {
        //         _foo = value;
        //         RaisePropertyChanged(() => Foo);
        //     }
        // }

        #endregion

        #region Commands

        private ICommand _addCoinCommand;
        public ICommand AddCoinCommand {
            get => _addCoinCommand ?? (_addCoinCommand = new RelayCommand(() => {
                CoinsManager.AddCoin(ToBeAddedCoinTitle, SelectedExchange.Name);
                CoinsManager.FillAllCoinsPricesWithLoading();
                //Foo = !Foo;
            }));
        }

        private ICommand _removeCoinCommand;
        public ICommand RemoveCoinCommand {
            get => _removeCoinCommand ?? (_removeCoinCommand = new RelayCommand(() => {
                CoinsManager.RemoveCoin(SelectedCoin);
            }));
        }

        private ICommand _readDataCommand;
        public ICommand ReadDataCommand {
            get => _readDataCommand ?? (_readDataCommand = new RelayCommand(() => {
                CoinsManager.Read();
                CoinsManager.FillAllCoinsPricesWithLoading();
            }));
        }

        private ICommand _writeDataCommand;
        public ICommand WriteDataCommand {
            get => _writeDataCommand ?? (_writeDataCommand = new RelayCommand(() => {
                CoinsManager.Write();
            }));
        }

        private ICommand _refreshCommand;
        public ICommand RefreshCommand {
            get => _refreshCommand ?? (_refreshCommand = new RelayCommand(() => {
                CoinsManager.FillAllCoinsPricesWithLoading();
            }));
        }

        #endregion
    }
}